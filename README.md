# Docker images for legacy Zope

## Legacy Zope 2.10

* [Ubuntu-18.04 base image](https://hub.docker.com/_/ubuntu)
* Python 2.4
  * bz2 module missing
  * psycopg2 2.4.6 linked to Ubuntu Postgres client library (Postgres 10.12)
  * 4SuiteXML 1.0.2
* Zope 2.10.13
  * installed in `/usr/local/zope/Zope-2.10.13`
  * runs as user "zope" (uid=1000, gid=1000)
  * runs on port 8080

## Legacy Zope 2.13

* [Python-2 base image](https://hub.docker.com/_/python)
* Python 2.7
  * psycopg2 (-binary version)
  * 4Suite-XML 1.0.2
  * Amara 1.2.0.2
  * PyJWT
  * httplib2
* Zope 2.13.30
  * Products.ZSQLMethods 2.13.5
  * ZPsycopgDA (from [psycopg/ZPsycopgDA](https://github.com/psycopg/ZPsycopgDA), needs compatible `ZPsycopgDA` in `Products` folder)
  * installed in `/usr/local/zope/Zope-2.13`
  * runs as user "zope" (uid=1000, gid=1000)
  * runs on port 8080

## Use image

Public images can be found at https://hub.docker.com/r/robcast/legacy-zope